<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Company</title>
</head>

<body>
    @if ($message = Session::get('success'))
        <div style="color: green">{{$message}}</div>
    @endif
    <div>
        <a href="{{ route('company.create') }}">tambah</a>
    </div>

    <table>
        <thead>
            <tr>
                <th>no</th>
                <th>nama</th>
                <th>alamat</th>

                <th>aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($company as $no => $item)
                <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>
                        <a href="{{ route('company.edit', $item->id) }}">Edit</a>
                        <form action="{{ route('company.destroy', $item->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="delete" />
                            <input type="submit" value="Hapus">
                        </form>

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
