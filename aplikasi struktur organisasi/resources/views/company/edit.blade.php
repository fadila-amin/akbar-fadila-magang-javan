<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>edit company</title>
</head>

<body>
    <form action="{{ route('company.update',$company->id) }}" method="POST">
        @csrf
        <input type="hidden" name="_method" value="put" />
        <label for="">Nama</label>
        <input type="text" name="nama" placeholder="Nama" value="{{$company->nama}}">
        <label for="">Alamat</label>
        <input type="text" name="alamat" placeholder="Alamat" value="{{$company->alamat}}">
        <input type="submit" value="simpan">
    </form>
</body>

</html>
