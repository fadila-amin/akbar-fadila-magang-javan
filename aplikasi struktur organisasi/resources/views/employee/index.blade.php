<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee</title>
</head>

<body>
    @if ($message = Session::get('success'))
        <div style="color: green">{{$message}}</div>
    @endif
    <div>
        <a href="{{ route('employee.create') }}">tambah</a>
    </div>

    <table>
        <thead>
            <tr>
                <th>no</th>
                <th>nama</th>
                <th>atasan</th>
                <th>company</th>
                <th>aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employee as $no => $item)
                <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $item->nama }}</td>
                    @if ($item->atasan)
                        <td>{{ $item->atasan->nama }}</td>
                    @else
                        <td>-</td>
                    @endif
                    <td>{{ $item->company->nama }}</td>
                    <td>
                        <a href="{{ route('employee.edit', $item->id) }}">Edit</a>
                        <form action="{{ route('employee.destroy', $item->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="delete" />
                            <input type="submit" value="Hapus">
                        </form>

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
