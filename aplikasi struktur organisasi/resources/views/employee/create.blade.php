<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tambah employee</title>
</head>

<body>
    <form action="{{ route('employee.store') }}" method="POST">
        @csrf
        <label for="">Nama</label>
        <input type="text" name="nama" placeholder="Nama Employee">
        <label for="">Atasan</label>
        <select name="atasan_id" id="atas">
            <option value="null">tidak memiliki atasan</option>
            @foreach ($atasan as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
        </select>
        <label for="">company</label>
        <select name="company_id" id="atas">
            @foreach ($company as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
        </select>
        <input type="submit" value="simpan">
    </form>
</body>

</html>
