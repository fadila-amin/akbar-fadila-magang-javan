<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>kalkulator</title>
    <style>
    html,
    body {
        height: 100%;
    }
    </style>
</head>

<body>

    <div class="d-flex align-items-center flex-column justify-content-center h-100  text-white" id="header">
        <div class="row">
            <div class="col-5">
                <input id="A" type="text" class="form-control">
            </div>
            <div class="col-2">
                <button id="tambah" type="button" class="btn btn-light m-1">+</button>
                <button id="kurang" type="button" class="btn btn-light m-1">-</button>
                <button id="kali" type="button" class="btn btn-light m-1">x</button>
                <button id="bagi" type="button" class="btn btn-light m-1">/</button>
            </div>
            <div class="col-5">
                <input id="B" type="text" class="form-control">
            </div>
        </div>
        <div class="">
            <h1 class="text-black-50" id="samdeng">=</h1>
        </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
    <script>
    $(document).ready(function() {
        $(document).on('click', '.btn', function() {
            let A = Number($('#A').val().trim());
            let B = Number($('#B').val().trim());
            let operan = $(this).text().trim();
            $.ajax({
                url: "{{url('kalkulator/hitung')}}" + '/' + A + '/' + B + '/' + (operan !==
                    '/' ?
                    operan : 'bagi'),
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    $('#samdeng').text('= ' + response.resp)
                }
            })
        })
    });
    </script>
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
</body>

</html>