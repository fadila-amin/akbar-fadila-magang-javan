<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Hello;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Hello::class, 'index']);

Route::view('/kalkulator', 'kalkulator');
Route::view('/gan-gen', 'ganGen');
Route::view('/vokal', 'vokal');

Route::get('/kalkulator/hitung/{A}/{B}/{operator}', 'Hello@kalkulator');
Route::post('/gan-gen/go', 'Hello@gangep');
Route::post('/vokal/go', 'Hello@hitungVokal');