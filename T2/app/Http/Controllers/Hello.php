<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Hello extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index()
    {
        return view('helloWorld');
    }


    function kalkulator($A, $B, $operator)
    {

        switch ($operator) {
            case '+':
                return response()->json([
                    'resp' =>   $A + $B
                ]);
                break;
            case '-':
                return response()->json([
                    'resp' =>   $A - $B
                ]);
                break;
            case 'x':
                return response()->json([
                    'resp' =>   $A * $B
                ]);
                break;
            case 'bagi':
                try {
                    return response()->json([
                        'resp' =>   $A / $B
                    ]);
                } catch (\DivisionByZeroError $e) {
                    return response()->json([
                        'resp' =>   'operasi tidak dapat dilakukan'
                    ]);
                };
                break;
            default:
                return response()->json([
                    'resp' =>   'operasi tidak memungkinkan'
                ]);
                break;
        }
    }
    function gangep(Request $req)
    {
        $A = $req->input('A');
        $B = $req->input('B');
        $container = array();
        for ($i = $A; $i <= $B; $i++) {
            if ($i % 2 != 0) {
                array_push($container, 'Angka ' . $i . ' adalah ganjil');
            } else {
                array_push($container, 'Angka ' . $i . ' adalah genap');
            }
        }
        return view('ganGen', ['data' => $container]);
    }
    function hitungVokal(Request $req)
    {
        $kata = strip_tags($req->input('kata'));

        if (!is_string($kata)) {
            return 'input buka string';
        } else {
            $vokal_group = array('a', 'i', 'u', 'e', 'o');
            $container = array();
            $kata = strtolower($kata);
            $pesan = '';
            foreach ($vokal_group as $item) {
                if (str_contains($kata, $item)) {
                    array_push($container, $item);
                }
            }
            if (count($container) == 0) {
                $pesan = $kata . ' tidak memiliki huruf vokal';
            } else {
                $pesan = "'" . $kata . "'" . ' memiliki ' . count($container) . ' huruf vokal, yaitu ';
                for ($i = 0; $i < count($container); $i++) {
                    if ($i == count($container) - 1) {
                        $pesan .= 'dan ';
                    }
                    $pesan .= "'" . $container[$i] . "' ";
                }
            }
        }
        return view('vokal', ['data' => $pesan]);
    }
}