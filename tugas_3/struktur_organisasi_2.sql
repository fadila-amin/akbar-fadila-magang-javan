-- CEO adalah orang yang posisinya tertinggi. Buat query untuk mencari siapa CEO
select `nama` from `employee` where atasan_id is null;

-- Staff biasa adalah orang yang tidak punya bawahan. Buat query untuk mencari siapa staff biasa
select nama from employee where atasan_id is not null and id not in(select distinct atasan_id from employee where atasan_id is not null)

-- Direktur adalah orang yang dibawah langsung CEO. Buat query untuk mencari siapa direktur
select nama from employee where atasan_id = 1

-- Manager adalah orang yang dibawah direktur dan di atas staff.  Buat query untuk mencari siapa manager
select nama from employee where atasan_id in (select id from employee where atasan_id = 1)

-- Buat sebuah query untuk mencari jumlah bawahan dengan parameter nama.
select  count(id) from employee
where atasan_id in (select id from employee where nama  = 'Pak Budi')
   or atasan_id in (select id from employee where atasan_id in (select id from employee where nama  = 'Pak Budi')
                                               or atasan_id in (select id from employee where atasan_id in (select id from employee where nama  = 'Pak Budi')))
