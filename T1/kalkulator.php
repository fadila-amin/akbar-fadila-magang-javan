<?php
function kalkulator($operasi)
{
    $operasi = str_replace(' ', '', $operasi);
    $operan1 = $operasi[0];
    $operan2 = $operasi[2];
    $operator = $operasi[1];

    switch ($operator) {
        case '+':
            echo $operan1 + $operan2;
            break;
        case '-':
            echo $operan1 - $operan2;
            break;
        case 'x':
            echo $operan1 * $operan2;
            break;
        case '/':
            try {
                echo $operan1 / $operan2;
            } catch (DivisionByZeroError $e) {
                echo 'tidak bisa dilakukan';
            };
            break;
        default:
            echo 'operasi tidak memungkinkan';
            break;
    }
}

// tes
kalkulator('2/0');