<?php
function ganjilGenap($A, $B)
{
    for ($i = $A; $i <= $B; $i++) {
        if ($i % 2 != 0) {
            echo 'Angka ' . $i . ' adalah ganjil <br/>';
        } else {
            echo 'Angka ' . $i . ' adalah genap <br/>', PHP_EOL;
        }
    }
}

ganjilGenap(1, 4);
echo '<br/>';

ganjilGenap(10, 11);
echo '<br/>';

ganjilGenap(6, 8);
echo '<br/>';