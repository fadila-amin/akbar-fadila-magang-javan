<?php
function hitungVokal($kata)
{
    if (!is_string($kata)) {
        echo 'input buka string';
    } else {
        $vokal_group = array('a', 'i', 'u', 'e', 'o');
        $container = array();
        $kata = strtolower($kata);

        foreach ($vokal_group as $item) {
            if (str_contains($kata, $item)) {
                array_push($container, $item);
            }
        }
        if (count($container) == 0) {
            echo $kata . ' tidak memiliki huruf vokal';
        } else {
            echo "'" . $kata . "'" . ' memiliki ' . count($container) . ' huruf vokal, yaitu ';
            for ($i = 0; $i < count($container); $i++) {
                if ($i == count($container) - 1) {
                    echo 'dan ';
                }
                echo "'" . $container[$i] . "' ";
            }
        }
    }
}

hitungVokal('aminudin');